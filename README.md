# Potatofric

## What is it

This project is a Slot Machine made with HTML5/CSS3/JS that can be seen live [here](https://www.potatofric.vivianetixier.com).


## How to build

 - Just launch the `index.html` file to see the project running


## License

This project is under MIT license. This means you can use it as you want (credit me please).
