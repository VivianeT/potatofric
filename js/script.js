// On cible les différents slot où vont être affichés les numéros
let $slot1 = document.querySelector('#slot1');
let $slot2 = document.querySelector('#slot2');
let $slot3 = document.querySelector('#slot3');

// On cible la div où sera marqué le résultat du jeu
let $resultGame = document.querySelector('#resultGame');

// On cible également le bouton pour jouer
let $playButton = document.querySelector('#playButton');

// On cible la div du score
let $divScore = document.querySelector('#score');
// On crée la variable du score
let $score = 100;

// On crée une fonction qui rafraichit la valeur de la divScore
function refreshScore() {
    $divScore.innerHTML = `Argent : ${$score} p$`;
}
// On l'insère dans la div
refreshScore();

//On crée un tableau contenant les numéros d'image, et leur nombre fera leur taux
//La valeur 1 a plus de chance de tomber que la 2 etc
let valuesArray = [1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 5];

//On crée une fonction qui permet de donner un slot au hasard du tableau
function random() {
    let randomValue = valuesArray[Math.floor(Math.random() * 20)];
    // On ne retourne pas directement la valeur, on retourne le html d'une image dont la source est
    // liée à la randomValue. 
    return `<img class="imgSlot" src="./img/${randomValue}.png" alt="${randomValue}"/>`;

}

// On va créer un écouteur d'évènement sur le clique du bouton qui appellera la fonction random
$playButton.addEventListener('click', function (e) {
    if ($score > 0) {
        // On perd un de score à chaque click
        $score = $score - 1;
        // On met à jour la div Score
        refreshScore();

        //On démarre la roulette avec des gif
        $slot1.innerHTML = `<img class="imgSlot" src="./img/slot1.gif"/>`;
        $slot2.innerHTML = `<img class="imgSlot" src="./img/slot2.gif"/>`;
        $slot3.innerHTML = `<img class="imgSlot" src="./img/slot3.gif"/>`;

        // On appelle la fonction random pour obtenir trois valeurs différents (ou pas ;) )
        let valueSlot1 = random();
        let valueSlot2 = random();
        let valueSlot3 = random();

        // On va insérer les valeurs dans le html pour que le joueur puisse les voir
        setTimeout(function () {
            $slot1.innerHTML = valueSlot1;
            $slot2.innerHTML = valueSlot2;
            $slot3.innerHTML = valueSlot3;


        // Il faut maintenant indiquer au joueur s'il a gagné ou perdu !
        if (valueSlot1 === `<img class="imgSlot" src="./img/1.png" alt="1"/>` && valueSlot1 === valueSlot2 && valueSlot1 === valueSlot3) {
            // Si les valeurs sont les mêmes, alors il a gagné
            $resultGame.innerHTML = "Bravo vous avez gagné ! Vous gagnez 5 p$ !";
            // Il gagne donc 5 points de score
            $score = $score + 5;
            //Et on met à jour la div score
            refreshScore();
        }
        else if (valueSlot1 === `<img class="imgSlot" src="./img/2.png" alt="2"/>` && valueSlot1 === valueSlot2 && valueSlot1 === valueSlot3) {
            // Si les valeurs sont les mêmes, alors il a gagné
            $resultGame.innerHTML = "Bravo vous avez gagné ! Vous gagnez 10 p$ !";
            // Il gagne donc 10 points de score
            $score = $score + 10;
            //Et on met à jour la div score
            refreshScore();
        }
        else if (valueSlot1 === `<img class="imgSlot" src="./img/3.png" alt="3"/>` && valueSlot1 === valueSlot2 && valueSlot1 === valueSlot3) {
            // Si les valeurs sont les mêmes, alors il a gagné
            $resultGame.innerHTML = "Bravo vous avez gagné ! Vous gagnez 20 p$ !";
            // Il gagne donc 20 points de score
            $score = $score + 20;
            //Et on met à jour la div score
            refreshScore();
        }
        else if (valueSlot1 === `<img class="imgSlot" src="./img/4.png" alt="4"/>` && valueSlot1 === valueSlot2 && valueSlot1 === valueSlot3) {
            // Si les valeurs sont les mêmes, alors il a gagné
            $resultGame.innerHTML = "Bravo vous avez gagné ! Vous gagnez 50 p$ !";
            // Il gagne donc 50 points de score
            $score = $score + 50;
            //Et on met à jour la div score
            refreshScore();
        }
        else if (valueSlot1 === `<img class="imgSlot" src="./img/5.png" alt="5"/>` && valueSlot1 === valueSlot2 && valueSlot1 === valueSlot3) {
            // Si les valeurs sont les mêmes, alors il a gagné
            $resultGame.innerHTML = "Bravo vous avez gagné ! Vous gagnez 100 p$ !";
            // Il gagne donc 100 points de score
            $score = $score + 100;
            //Et on met à jour la div score
            refreshScore();
        }
        else {
            // Sinon, c'est perdu
            $resultGame.innerHTML = "Perdu, réessaie !";
        }
        }, 1000);

    }
    else {
        // Une fois que le joueur n'a plus d'argent, on fait apparaitre un bouton reset
        $resultGame.innerHTML = `Tu n'as plus d'argent, désolé ! <button id="resetButton">Reset</button>`
    }

    // On cible le bouton reset
    let $resetButton = document.querySelector('#resetButton');

    // On place un écouteur d'évènement sur le click de ce bouton pour remettre à zéro !
    if ($resetButton === true) {
        $resetButton.addEventListener('click', function (e) {
            // On remet le score à 100
            $score = 100;
            // On met à jour la div score
            refreshScore();
            // On rechanger le texte du result
            $resultGame.innerHTML = "A toi de jouer !"
        });
    }

});
